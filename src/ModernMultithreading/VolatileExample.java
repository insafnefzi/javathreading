package ModernMultithreading;




/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Insaf-Nefzi
 */
public class VolatileExample {
    
    public static volatile  boolean flag= false;
    
    public static void main(String[] args) {
        new Thread(()-> {
          for (int i=0; i<2000; i++){
              System.out.println("Value of i is : "+ i);
          }
          flag = true;
            System.out.println("the value of flag is : "+ flag);
        }).start();
        
        new Thread(()-> {
          int i=1;
          while(!flag){
              ++i;
          }
            System.out.println("Value of i in the second thread is : "+ i);
        }).start();
    }
}
