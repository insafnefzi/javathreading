/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModernMultithreading;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Insaf-Nefzi
 */
public class FastFoodRestaurantSynchronizedBlock {
    
    private String lastClientName;
    private int numberOfBurgersSold;
    
    public  void  buyBurger(String clientName){
        alongRunningProcess();
        synchronized(this){
        this.lastClientName=clientName;
        numberOfBurgersSold++;
        System.out.println(clientName +" Bought a burger");
        }
    }
    
    public void alongRunningProcess() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
            Logger.getLogger(FastFoodRestaurantSynchronizedBlock.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    public String getLastClientName() {
        return lastClientName;
    }

    public int getNumberOfBurgersSold() {
        return numberOfBurgersSold;
    }

    public void setLastClientName(String lastClientName) {
        this.lastClientName = lastClientName;
    }

    public void setNumberOfBurgersSold(int numberOfBurgersSold) {
        this.numberOfBurgersSold = numberOfBurgersSold;
    }
    
    public static void main(String[] args) throws InterruptedException {
        long startTime = System.currentTimeMillis();
        FastFoodRestaurantSynchronizedBlock fastFoodRestaurant = new FastFoodRestaurantSynchronizedBlock();
        Thread t1= new Thread (()->{
            fastFoodRestaurant.buyBurger("Mike");
        });
        Thread t2= new Thread (()->{
            fastFoodRestaurant.buyBurger("Syed");
        });
        Thread t3= new Thread (()->{
            fastFoodRestaurant.buyBurger("Jean");
        });
        Thread t4= new Thread (()->{
            fastFoodRestaurant.buyBurger("Amy");
        });
        
        t1.start();
        t2.start();
        t3.start();
        t4.start();
        
        t1.join();
        t2.join();
        t3.join();
        t4.join();
        
        System.out.println("Total number of burgers sold: "  +fastFoodRestaurant.getNumberOfBurgersSold());
        System.out.println("The last name of client is "+ fastFoodRestaurant.getLastClientName());
        System.out.println("Total execution time: "+ (System.currentTimeMillis() - startTime) +" im msec");
    }
}
